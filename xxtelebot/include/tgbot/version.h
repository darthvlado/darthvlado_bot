#ifndef TGBOT_VERSION_H
#define TGBOT_VERSION_H

/*!
 * @brief Major version
 */
constexpr int TGBOT_VERSION_MAJOR = 1;

/*!
 * @brief Minor version
 */
constexpr int TGBOT_VERSION_MINOR = 4;

/*!
 * @brief Patch version
 */
constexpr int TGBOT_VERSION_PATCH = 1;

/*!
 * @brief Tweak version
 */
constexpr int TGBOT_VERSION_TWEAK = 0;

/*!
 * @brief Telegram Bot API major version
 */
constexpr int TELEGRAM_BOT_API_VERSION_MAJOR = 4;

/*!
 * @brief Telegram Bot API minor version
 */
constexpr int TELEGRAM_BOT_API_VERSION_MINOR = 3;

#endif
