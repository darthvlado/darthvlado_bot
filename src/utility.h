#pragma once

#include "json.hpp"
#include <tgbot/bot.h>

#include <codecvt>

void log(const tgbot::types::Message& message, bool enable_log_file = true, bool is_anon_chat = false);

int mod(int divident, int divisor);

bool is_admin(const tgbot::types::Message& message, const tgbot::methods::Api& api);

bool is_bot_admin(const tgbot::types::Message& message, const tgbot::methods::Api& api);

bool is_this_bot(const tgbot::types::Message& message, const tgbot::methods::Api& api);

bool is_reply(const tgbot::types::Message& message);

void cut_command_message(const tgbot::types::Message& message);

std::string get_any_name(const tgbot::types::Message& message);

std::string get_mention(const tgbot::types::Message& message, std::string mention = "");

std::size_t get_length(std::string& str);

void update_database();
