#pragma once

#include <tgbot/bot.h>

#include <string>

void help_latex(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void clear(const std::string& filename);

std::string getLatexImg(const std::string& code, bool isEquation = true);

void inlineQuery(const tgbot::types::InlineQuery& query, const tgbot::methods::Api& api);

void latex(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);
