#pragma once

#include <tgbot/bot.h>

void welcome_set(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void welcome_switch(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);