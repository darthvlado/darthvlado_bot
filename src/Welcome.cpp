#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "Welcome.h"

#include "utility.h"
#include "database.h"

using namespace tgbot;
using namespace utils;
using namespace types;
using namespace methods;
using namespace methods::types;
using namespace std;

void welcome_set(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_bot_admin(message, api) || !is_admin(message, api))
            return;
        if (message.chat.type == ChatType::PRIVATE)
            return;

        cut_command_message(message);

        db["chats"][to_string(message.chat.id)]["welcome_message"] = *message.text;
        update_database();

        api.sendMessage(
                to_string(message.chat.id),
                "Установлено приветствие: \"" + *message.text + '\"'
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void welcome_switch(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_bot_admin(message, api) || !is_admin(message, api))
            return;
        if (message.chat.type == ChatType::PRIVATE)
            return;

        bool state = db["chats"][to_string(message.chat.id)]["welcome_enabled"] == nullptr
                     ? false
                     : db["chats"][to_string(message.chat.id)]["welcome_enabled"].get<bool>();

        if (state) {
            db["chats"][to_string(message.chat.id)]["welcome_enabled"] = false;
            api.sendMessage(
                    to_string(message.chat.id),
                    "Приветствие выключено"
            );
        } else {
            db["chats"][to_string(message.chat.id)]["welcome_enabled"] = true;

            string str;
            if (db["chats"][to_string(message.chat.id)]["welcome_message"] == nullptr) {
                str = "Привет!";
                db["chats"][to_string(message.chat.id)]["welcome_message"] = str;

            } else {
                str = db["chats"][to_string(message.chat.id)]["welcome_message"].get<string>();
            }

            api.sendMessage(
                    to_string(message.chat.id),
                    "Приветствие включено, текущее приветствие: \"" + str + "\""
            );
        }

        update_database();

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

#pragma GCC diagnostic pop