#pragma once

#include <tgbot/bot.h>

void kick(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void ban(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void mute(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void unmute(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void approve(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);
