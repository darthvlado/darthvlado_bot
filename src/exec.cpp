#include "exec.h"

#include <memory>
#include <cerrno>
#include <unistd.h>

using namespace std;

string exec(const vector<string>& args, bool inc_stderr, volatile pid_t* pd) {
    int stdout_fds[2];
    pipe(stdout_fds);

    int stderr_fds[2];
    if (!inc_stderr) {
        pipe(stderr_fds);
    }

    pid_t pid = fork();
    if (pd != nullptr) {
        *pd = pid;
    }
    if (!pid) {
        close(stdout_fds[0]);
        dup2(stdout_fds[1], 1);
        if (inc_stderr) {
            dup2(stdout_fds[1], 2);
        }

        close(stdout_fds[1]);

        if (!inc_stderr) {
            close(stderr_fds[0]);
            dup2(stderr_fds[1], 2);
            close(stderr_fds[1]);
        }

        vector<char*> vc(args.size() + 1, nullptr);
        for (size_t i = 0; i < args.size(); ++i) {
            vc[i] = const_cast<char*>(args[i].c_str());
        }

        execvp(vc[0], &vc[0]);
        exit(0);
    }

    close(stdout_fds[1]);

    string out;
    const int buf_size = 4096;
    char buffer[buf_size];
    do {
        const ssize_t r = read(stdout_fds[0], buffer, buf_size);
        if (r > 0) {
            out.append(buffer, r);
        }
    } while (errno == EAGAIN || errno == EINTR);

    close(stdout_fds[0]);

    if (!inc_stderr) {
        close(stderr_fds[1]);
        do {
            read(stderr_fds[0], buffer, buf_size);
        } while (errno == EAGAIN || errno == EINTR);

        close(stderr_fds[0]);
    }

    int r, status;
    do {
        r = waitpid(pid, &status, 0);
    } while (r == -1 && errno == EINTR);

    return out;
}

string exec_unsafe(const string& command) {
    const char* cmd = command.c_str();
    array<char, 128> buffer{};
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen((string(cmd) + " 2>&1").c_str(), "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }

    result.erase(result.find_last_not_of(" \n\r\t") + 1);
    return result;
}