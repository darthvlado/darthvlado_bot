#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Admin.h"

#include "utility.h"
#include "database.h"

using namespace tgbot;
using namespace utils;
using namespace types;
using namespace methods;
using namespace methods::types;
using namespace std;

void kick(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_reply(message)
            || is_this_bot(*message.replyToMessage, api)
            || !is_admin(message, api)
            || is_admin(*message.replyToMessage, api)
            || !is_bot_admin(message, api))
            return;

        api.sendMessage(
                to_string(message.chat.id),
                get_mention(*message.replyToMessage) + " отправлен\\_а пинком из чата восвояси! Но может вернуться.",
                ParseMode::MARKDOWN
        );

        api.kickChatMember(
                to_string(message.chat.id),
                message.replyToMessage->from->id,
                0
        );
        api.unbanChatMember(
                to_string(message.chat.id),
                message.replyToMessage->from->id
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void ban(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_reply(message)
            || is_this_bot(*message.replyToMessage, api)
            || !is_admin(message, api)
            || is_admin(*message.replyToMessage, api)
            || !is_bot_admin(message, api))
            return;

        cut_command_message(message);
        //bool forever = false;
        //int days = message.text->empty() ? -1 : stoi(*message.text);
        //if (days <= 0)
        //    forever = true;

//        if (forever) {
            api.kickChatMember(
                    to_string(message.chat.id),
                    message.replyToMessage->from->id,
                    0
            );
            api.sendMessage(
                    to_string(message.chat.id),
                    get_mention(*message.replyToMessage) + " отправлен\\_а в бан навеки.",
                    ParseMode::MARKDOWN
            );
//        } else {
//            api.kickChatMember(
//                    to_string(message.chat.id),
//                    message.replyToMessage->from->id,
//                    60 * 24 * days + time(nullptr)
//            );
//            api.sendMessage(
//                    to_string(message.chat.id),
//                    get_mention(*message.replyToMessage) + " отправлен\\_а в бан на " + to_string(days) + " дней",
//                    ParseMode::MARKDOWN
//            );
//        }

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void mute(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_reply(message)
            || is_this_bot(*message.replyToMessage, api)
            || !is_admin(message, api)
            || is_admin(*message.replyToMessage, api)
            || !is_bot_admin(message, api))
            return;

        cut_command_message(message);
        int minutes = message.text->empty() ? 5 : stoi(*message.text);
        if (minutes <= 0)
            return;

        api.sendMessage(
                to_string(message.chat.id),
                get_mention(*message.replyToMessage) + " помолчит " + to_string(minutes) + " мин.",
                ParseMode::MARKDOWN
        );

        api.restrictChatMember(
                to_string(message.chat.id),
                message.replyToMessage->from->id,
                ChatMemberRestrict{
                        false,
                        false,
                        false,
                        false},
                60 * minutes + time(nullptr)
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }

}

void unmute(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_reply(message)
            || is_this_bot(*message.replyToMessage, api)
            || !is_admin(message, api)
            || is_admin(*message.replyToMessage, api)
            || !is_bot_admin(message, api))
            return;

        api.restrictChatMember(
                to_string(message.chat.id),
                message.replyToMessage->from->id,
                ChatMemberRestrict{
                        true,
                        true,
                        true,
                        true},
                0
        );

        if (is_bot_admin(message, api)) {
            api.deleteMessage(
                    to_string(message.chat.id),
                    to_string(message.messageId)
            );
        }

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void approve(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_reply(message)
            || is_this_bot(*message.replyToMessage, api)
            || !is_admin(message, api)
            || is_admin(*message.replyToMessage, api)
            || !is_bot_admin(message, api))
            return;

        vector<int32_t> members = db["chats"][to_string(message.chat.id)]["known_members"] == nullptr
                                  ? vector<int32_t>()
                                  : db["chats"][to_string(message.chat.id)]["known_members"].get<vector<int32_t>>();
        members.push_back(message.replyToMessage->from->id);
        db["chats"][to_string(message.chat.id)]["known_members"] = members;
        update_database();


        api.sendMessage(
                to_string(message.chat.id),
                get_mention(*message.replyToMessage) + " допущен\\_а. Приветствуем!",
                ParseMode::MARKDOWN
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

#pragma GCC diagnostic pop