#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "Common.h"

#include "database.h"
#include "utility.h"
#include "exec.h"

#include <boost/algorithm/string.hpp>
#include <random>
#include <sstream>
#include <thread>
#include <future>
#include <filesystem>
#include <fstream>

using namespace tgbot;
using namespace utils;
using namespace types;
using namespace methods;
using namespace methods::types;
using namespace std;

void start(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        api.sendMessage(
                to_string(message.chat.id),
                "Я работаю!"
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void help_bot(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        api.sendMessage(
                to_string(message.chat.id),
                /*R"(
/start - проверка, не упал ли бот
/help_latex - справка по LaTeX
/help_dice - справка по дайсовой нотации
/or - [a, b...] выбор случайного варианта
/roll - [N] случайное целое число от 0 до N
/dice - [PATTERN] бросить кости
/truth - проверка истинности фразы
/me - [MSG] сказать от третьего лица
/latex - [CODE] LaTeX
/latexeq - [CODE] LaTeX с окружением equation
/s/PATTERN/REPLACEMENT - sed (замена)

Для админов:
/kick - пнуть пользователя из чата
/ban - забанить
/mute - [N] залушить на N минут
/unmute - снять заглушение
/set_welcome - [MSG] настроить приветствие
/switch_welcome - вкл/выкл приветствие
/approve - подтвердить
/set_random_mutes - настроить случайные заглушения
/switch_random_mutes - вкл/выкл случайные заглушения

Только в личных сообщениях:
/switch_anon_chat - вкл/выкл анонимный чат
)"
        );*/
                R"(
/start - проверка, не упал ли бот
/help_bot - справка по командам этого бота
/help_latex - справка по LaTeX
/latex - [CODE] LaTeX
@darthvlado_bot - LaTeX (inline equation)
/s/PATTERN/REPLACEMENT - sed (замена)
/or - [a, b...] выбор случайного варианта
/truth - проверка истинности фразы
/me - [MSG] сказать от третьего лица

Для админов:
/kick - пнуть пользователя из чата
/ban - забанить
/mute - [N] залушить на N минут
/unmute - снять заглушение
/approve - подтвердить при входе

/welcome_switch - вкл/выкл приветствие
/welcome_set - [MSG] настроить приветствие
/auto_mutes_switch - вкл/выкл случайные заглушения
/auto_mutes_set - настроить случайные заглушения
)"
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void me(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (is_bot_admin(message, api)) {
            api.deleteMessage(
                    to_string(message.chat.id),
                    to_string(message.messageId)
            );
        }
        string mention = get_mention(message);
        boost::replace_all(*message.text, "/me", mention);
        api.sendMessage(
                to_string(message.chat.id),
                *message.text,
                ParseMode::MARKDOWN
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void truth(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_reply(message))
            return;

        int k = 0;
        for (char i : *message.replyToMessage->text) {
            k += int(i);
        }
        k = mod(k, 101);

        if (is_bot_admin(message, api)) {
            api.deleteMessage(
                    to_string(message.chat.id),
                    to_string(message.messageId)
            );
        }
        api.sendMessage(
                to_string(message.chat.id),
                "Вероятность этого: " + to_string(k) + "%. " + get_mention(message) + ", убедился?",
                message.replyToMessage->messageId,
                ParseMode::MARKDOWN
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void onMessage(const Message& message, const Api& api) {
    try {
        log(message);

        // random mutes
        bool random_mutes_enabled{
                db["chats"][to_string(message.chat.id)]["random_mutes_enabled"] == nullptr
                ? false
                : db["chats"][to_string(message.chat.id)]["random_mutes_enabled"].get<bool>()
        };

        if (random_mutes_enabled && !is_admin(message, api)) {
            default_random_engine re{uint64_t(time(nullptr))};
            uniform_real_distribution<double> random(0., 100.);
            double rnd = random(re);
            double probability_factor = exp(-1. / 8 * get_length(*message.text)) / exp(-20. / 8);
            double probability =
                    db["chats"][to_string(message.chat.id)]["random_mutes_probability"].get<double>() *
                    probability_factor;
            if (rnd <= probability) {
                uniform_int_distribution<int> seconds(
                        db["chats"][to_string(message.chat.id)]["random_mutes_min_seconds"].get<int>(),
                        db["chats"][to_string(message.chat.id)]["random_mutes_max_seconds"].get<int>()
                );
                int sec = seconds(re);

                if (probability > 100)
                    probability = 100;

                api.sendMessage(
                        to_string(message.chat.id),
                        get_mention(message) + ", ты заглушен\\_а на " + to_string(sec)
                        + " с. (" + to_string(probability) + "%)",
                        message.messageId,
                        ParseMode::MARKDOWN
                );

                api.restrictChatMember(
                        to_string(message.chat.id),
                        message.from->id,
                        ChatMemberRestrict{
                                0,
                                0,
                                0,
                                0},
                        sec + time(nullptr)
                );
            }
        }

        //new members
        if (message.newChatMembers != nullptr && !message.newChatMembers->empty()) {
            if (message.newChatMembers->front().isBot) {
                api.sendMessage(
                        to_string(message.chat.id),
                        "БОТ! БОТ! --> @" + *message.newChatMembers->front().username + " <-- это бот!",
                        message.messageId
                );
                api.sendMessage(
                        to_string(message.chat.id),
                        "Дадим Скайнету бой! Бот, бот, бот! Здесь появился бот!"
                );
                return;
            }

            // AYSEL COUNTER
            if (message.newChatMembers->front().id == 299680495 && message.chat.id == -1001231540042) {
                int count = db["chats"]["-1001231540042"]["Aysel_leave_count"].get<int>();
                ++count;
                api.sendMessage(
                        to_string(message.chat.id),
                        "ВНИМАНИЕ, ВНИМАНИЕ, ВНИМАНИЕ! Она здесь! Айсель снова в этом чате уже как минимум " +
                        to_string(count) + " раз_а!",
                        message.messageId
                );
                db["chats"]["-1001231540042"]["Aysel_leave_count"] = count;
                update_database();
            }

            bool welcome_enabled = db["chats"][to_string(message.chat.id)]["welcome_enabled"] == nullptr
                         ? false
                         : db["chats"][to_string(message.chat.id)]["welcome_enabled"].get<bool>();

            if (!welcome_enabled)
                return;

            vector<int32_t> members = db["chats"][to_string(message.chat.id)]["known_members"] == nullptr
                                      ? vector<int32_t>()
                                      : db["chats"][to_string(message.chat.id)]["known_members"]
                                              .get<vector<int32_t>>();

            if (find(members.begin(), members.end(), message.newChatMembers->front().id) != members.end())
                return;

            auto auto_kick = [&api](int64_t chat_id, int32_t user_id) {
                for (int t = 0; t < 300; ++t) {
                    this_thread::sleep_for(chrono::seconds(1));
                    auto members = db["chats"][to_string(chat_id)]["known_members"] == nullptr
                                   ? vector<int32_t>()
                                   : db["chats"][to_string(chat_id)]["known_members"]
                                           .get<vector<int32_t>>();
                    if (find(members.begin(), members.end(), user_id) != members.end()) {
                        return;
                    }
                }

                auto member = api.getChatMember(to_string(chat_id), user_id);
                if (member.status != ChatMemberStatus::KICKED || member.status != ChatMemberStatus::LEFT) {
                    api.kickChatMember(
                            to_string(chat_id),
                            user_id,
                            0
                    );
                    api.unbanChatMember(
                            to_string(chat_id),
                            user_id
                    );
                }
            };

            thread kicker(auto_kick, message.chat.id, message.newChatMembers->front().id);
            kicker.detach();

            api.sendMessage(
                    to_string(message.chat.id),
                    db["chats"][to_string(message.chat.id)]["welcome_message"].get<string>(),
                    message.messageId
            );
        }

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void auto_mutes_switch(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_bot_admin(message, api) || !is_admin(message, api))
            return;
        if (message.chat.type == ChatType::PRIVATE)
            return;

        bool random_mutes_enabled = db["chats"][to_string(message.chat.id)]["random_mutes_enabled"] == nullptr
                                    ? false
                                    : db["chats"][to_string(message.chat.id)]["random_mutes_enabled"].get<bool>();

        if (!random_mutes_enabled) {
            if (db["chats"][to_string(message.chat.id)]["random_mutes_probability"] == nullptr) {
                db["chats"][to_string(message.chat.id)]["random_mutes_probability"] = 0.5;
                db["chats"][to_string(message.chat.id)]["random_mutes_min_seconds"] = 45;
                db["chats"][to_string(message.chat.id)]["random_mutes_max_seconds"] = 120;
            }

            api.sendMessage(
                    to_string(message.chat.id),
                    "Включены случайные заглушения с вероятностью "
                    + to_string(db["chats"][to_string(message.chat.id)]["random_mutes_probability"].get<double>())
                    + "% от "
                    + to_string(db["chats"][to_string(message.chat.id)]["random_mutes_min_seconds"].get<int>())
                    + " секунд до "
                    + to_string(db["chats"][to_string(message.chat.id)]["random_mutes_max_seconds"].get<int>())
                    + " секунд!"
            );

        } else {
            api.sendMessage(
                    to_string(message.chat.id),
                    "Случайные заглушения отключены."
            );
        }

        db["chats"][to_string(message.chat.id)]["random_mutes_enabled"] = !random_mutes_enabled;
        update_database();

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void auto_mutes_set(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (!is_bot_admin(message, api) || !is_admin(message, api))
            return;
        if (message.chat.type == ChatType::PRIVATE)
            return;

        cut_command_message(message);

        double random_mutes_probability = 0;
        int random_mutes_min_seconds = 0;
        int random_mutes_max_seconds = 0;

        stringstream ss(*message.text);
        ss >> random_mutes_probability;
        ss >> random_mutes_min_seconds;
        ss >> random_mutes_max_seconds;

        if (random_mutes_probability <= 0 || random_mutes_probability > 100
            || random_mutes_min_seconds < 35 || random_mutes_min_seconds > random_mutes_max_seconds
                ) {
            api.sendMessage(
                    to_string(message.chat.id),
                    "Вы должны ввести параметры в следующем порядке: вероятность, минимальное количество секунд "
                    + string("(не менее 35), максимальное количество секунд. Попробуйте снова.")
            );
            return;
        }

        db["chats"][to_string(message.chat.id)]["random_mutes_probability"] = random_mutes_probability;
        db["chats"][to_string(message.chat.id)]["random_mutes_min_seconds"] = random_mutes_min_seconds;
        db["chats"][to_string(message.chat.id)]["random_mutes_max_seconds"] = random_mutes_max_seconds;

        api.sendMessage(
                to_string(message.chat.id),
                "Установлены параметры случайного заглушения: вероятность "
                + to_string(db["chats"][to_string(message.chat.id)]["random_mutes_probability"].get<double>())
                + "% от "
                + to_string(db["chats"][to_string(message.chat.id)]["random_mutes_min_seconds"].get<int>())
                + " секунд до "
                + to_string(db["chats"][to_string(message.chat.id)]["random_mutes_max_seconds"].get<int>())
                + " секунд."
        );

        update_database();

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void s(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (message.replyToMessage != nullptr) {
            vector<string> patterns;
            stringstream ss(*message.text);
            string to;

            while (getline(ss, to, '\n')) {
                string pattern = "s" + to.erase(0, 2) + "/g";
                patterns.push_back(pattern);
            }
            string result = *message.replyToMessage->text;

            if (is_bot_admin(message, api)) {
                api.deleteMessage(
                        to_string(message.chat.id),
                        to_string(message.messageId)
                );
            }

            for (const auto& pattern : patterns) {
                auto filename =
                        to_string(chrono::system_clock::now().time_since_epoch().count()) + "-sed.txt";
                auto path = filesystem::path(filesystem::temp_directory_path().string() + '/' + filename);
                ofstream file(path, ios::trunc | ios::out);
                file << result;
                file.close();

                volatile pid_t pid = 0;
                auto p_pid = &pid;

                future<string> future = async(launch::async, [&pattern, &path, &p_pid]() {
                    return exec(vector<string>{"/bin/sed", "--sandbox", "-e", pattern, path}, true, p_pid);
                });

                future_status status = future.wait_for(chrono::seconds(2));
                if (status == future_status::timeout) {
                    kill(pid, SIGKILL);
                    throw invalid_argument("timeout");
                }

                result = future.get();
                filesystem::remove(path);
            }

            api.sendMessage(
                    to_string(message.chat.id),
                    result,
                    message.replyToMessage->messageId
            );
        }

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void roll(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        cut_command_message(message);
        default_random_engine re{uint64_t(time(nullptr))};
        int max;
        try {
            max = stoi(*message.text);
        } catch (const std::invalid_argument& ia) {
            return;
        }

        if (max < 0)
            return;

        uniform_int_distribution<int> uni(0, max);

        api.sendMessage(
                to_string(message.chat.id),
                to_string(uni(re)),
                message.messageId
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void _or(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        cut_command_message(message);
        default_random_engine re{uint64_t(time(nullptr))};
        vector<string> variants;

        stringstream ss(*message.text);
        string value;
        while (getline(ss, value, ',')) {
            boost::algorithm::trim(value);
            variants.push_back(value);
        }

        if (variants.empty())
            return;

        uniform_int_distribution<int> uni(0, variants.size() - 1);

        api.sendMessage(
                to_string(message.chat.id),
                variants[uni(re)],
                message.messageId
        );

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

#pragma GCC diagnostic pop