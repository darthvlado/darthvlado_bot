#include "Common.h"
#include "LaTeX.h"
#include "Admin.h"
#include "Welcome.h"

#include "database.h"
#include "json.hpp"
#include <tgbot/bot.h>

#include <iostream>
#include <fstream>

using namespace tgbot;
using namespace utils;
using namespace types;
using namespace methods;
using namespace methods::types;
using namespace std;

int main() {
    ifstream db_file("database.json");
    db_file >> db;
    db_file.close();

    // bot
    LongPollBot bot(db["token"].get<string>());
    cout << "bot username: " << *bot.getMe().username << endl;

    bot.callback(whenStarts, start, "/start");
    bot.callback(whenStarts, help_bot, "/help_bot");
    bot.callback(whenStarts, me, "/me");
    bot.callback(whenStarts, truth, "/truth");
    bot.callback(whenStarts, s, "/s");
    bot.callback(whenStarts, roll, "/roll ");
    bot.callback(whenStarts, _or, "/or ");

    bot.callback(inlineQuery);
    bot.callback(whenStarts, help_latex, "/help_latex");
    bot.callback(whenStarts, latex, "/latex ");

    bot.callback(whenStarts, kick, "/kick");
    bot.callback(whenStarts, ban, "/ban");
    bot.callback(whenStarts, mute, "/mute");
    bot.callback(whenStarts, unmute, "/unmute");
    bot.callback(whenStarts, approve, "/approve");

    bot.callback(whenStarts, auto_mutes_switch, "/auto_mutes_switch");
    bot.callback(whenStarts, auto_mutes_set, "/auto_mutes_set ");
    bot.callback(whenStarts, welcome_switch, "/welcome_switch");
    bot.callback(whenStarts, welcome_set, "/welcome_set ");

    bot.callback(onMessage);

    std::ofstream outfile("log/error_log.txt", ios::app | ios::out);
    bot.getLogger().setStream(outfile);
    bot.start();
}
