#pragma once

#include <tgbot/bot.h>

void start(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void help_bot(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void me(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void truth(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void onMessage(const tgbot::types::Message& message, const tgbot::methods::Api& api);

void auto_mutes_switch(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void auto_mutes_set(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void s(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void roll(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);

void _or(const tgbot::types::Message& message, const tgbot::methods::Api& api, const std::vector<std::string>& args);