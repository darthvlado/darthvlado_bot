#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "LaTeX.h"

#include "exec.h"
#include "utility.h"
#include <tgbot/utils/make_ptr.h>

#include <boost/algorithm/string.hpp>
#include <future>
#include <thread>
#include <filesystem>
#include <fstream>
#include <string>

using namespace tgbot;
using namespace utils;
using namespace types;
using namespace methods;
using namespace methods::types;
using namespace std;

vector<string> banned{
        "\\input",
        "\\immediate",
        "\\write18"
        //"\\newcommand",
        //"\\renewcommand",
        //"\\NewDocumentCommand",
        //"\\newenvironment"
};

void help_latex(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        api.sendMessage(
                to_string(message.chat.id),
                R"(
СПРАВКА ПО LATEX:

https://en.wikibooks.org/wiki/LaTeX/Mathematics
http://mirrors.ibiblio.org/CTAN/macros/latex/contrib/physics/physics.pdf

В преамбуле установлены amsmath, amssymb, physics, tikz и некоторые другие пакеты.
• @darthvlado_bot - содержимое будет помещено в {equation}.
• /latex - содержимое будет помещено в тело документа.

Также для удобства добавлены некоторые дополнительные команды:
• \const - константа
• \opgrad - градиент
• \opdiv - дивергенция
• \oprot - ротор
• \oplaplace - оператор Лапласа
• \opdalembert - оператор Даламбера

Добавлены интегралы, более близкие к русской типографской традиции:
• \rint, \riint, \riiint - интегралы
• \roint, \roiint, \roiiint - интегралы по замкнутому контуру
)"
        );
    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void clear(const string& filename) {
    filesystem::remove(filename + ".jpg");
    filesystem::remove(filename + ".tex");
    filesystem::remove(filename + ".log");
    filesystem::remove(filename + ".aux");
    filesystem::remove(filename + ".pdf");
}

string getLatexImg(const string& code, bool isEquation) {
    try {
        ifstream preambule("../src/latex-preambule.txt", ios::in);
        string filename = to_string(time(nullptr)) + "-latex";
        ofstream tex(filename + ".tex", ios::trunc | ios::out);

        string str;
        while (getline(preambule, str)) {
            tex << str << '\n';
        }
        preambule.close();

        if (isEquation) {
            tex << " \\begin{document} \\begin{equation*} " << '\n';
            tex << code << '\n';
            tex << " \\end{equation*} \\end{document} " << '\n';
        } else {
            tex << " \\begin{document} \n" << '\n';
            tex << code << '\n';
            tex << " \\end{document} " << '\n';
        }
        tex.close();

        future<string> future = async(launch::async, [&filename]() {
            return exec_unsafe("pdflatex --no-shell-escape -halt-on-error " + filename + ".tex");
        });

        future_status status = future.wait_for(chrono::seconds(20));
        string result = future.get();

        if (status == future_status::timeout) {
            system("killall pdflatex");
            clear(filename);
            throw invalid_argument("timeout");
        }
        if (!filesystem::exists(filename + ".pdf")) {
            clear(filename);
            throw invalid_argument("latex error");
        }

        system(("convert -density 1200 " + filename
                + ".pdf -quality 100 -flatten -sharpen 0x1.0 -trim " + filename + ".jpg").c_str());
        return filename;

    } catch (const invalid_argument& e) {
        cerr << e.what() << '\n';
        return "";
    } catch (const exception& e) {
        cerr << e.what() << '\n';
        return "";
    } catch (...) {
        cerr << "unknown exception\n";
        return "";
    }
}

void inlineQuery(const InlineQuery& query, const Api& api) {
    try {
        if (query.query.empty())
            throw invalid_argument("empty message");

        for (const auto& i : banned) {
            if (query.query.find(i) != string::npos) {
                string name = query.from.username->empty() ? "anonymous" : *query.from.username;
                throw invalid_argument(to_string(query.from.id) + '\t' + name + " ATTEMPTED TO ACCESS THROUGH LATEX");
            }
        }

        tgbot::methods::types::InlineQueryResultsVector result;
        Ptr<InlineQueryResultCachedPhoto> ph = makePtr<InlineQueryResultCachedPhoto>();
        ph = tgbot::utils::makePtr<tgbot::methods::types::InlineQueryResultCachedPhoto>();

        string filename;
        //if (query.query.rfind("/raw", 0) == 0) {
        //    filename = getLatexImg(query.query.substr(4), false);
        //} else {
            filename = getLatexImg(query.query, true);
        //}
        if (filename.empty())
            return;

        auto photo_cache_msg = api.sendPhoto(
                to_string(-1001470222882),
                filename + ".jpg",
                FileSource::LOCAL_UPLOAD,
                "image/jpeg"
        );
        clear(filename);

        ph->type = iqrTypePhoto;
        ph->photoFileId = photo_cache_msg.photo->at(0).fileId;
        ph->id = "0";
        string caption(query.query);
        boost::replace_all(caption, R"(\)", R"(\\)");
        ph->caption = makePtr<string>(caption);
        result.push_back(std::move(ph));

        api.answerInlineQuery(query.id, result);

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

void latex(const Message& message, const Api& api, const vector<string>& args) {
    try {
        log(message);

        if (is_bot_admin(message, api)) {
            api.deleteMessage(
                    to_string(message.chat.id),
                    to_string(message.messageId)
            );
        }

        if (message.text->empty())
            throw invalid_argument("empty message");

        for (const auto& i : banned) {
            if (message.text->find(i) != string::npos) {
                string name = message.from->username->empty() ? "anonymous" : *message.from->username;
                throw invalid_argument(
                        to_string(message.from->id) + '\t' + name + " ATTEMPTED TO ACCESS THROUGH LATEX");
            }
        }

        cut_command_message(message);
        string filename = getLatexImg(*message.text, false);
        if (filename.empty())
            return;

        api.sendPhoto(
                to_string(message.chat.id),
                filename + ".jpg",
                FileSource::LOCAL_UPLOAD,
                "image/jpeg",
                *message.text
        );

        clear(filename);

    } catch (const exception& e) {
        cerr << e.what() << '\n';
        api.getLogger().error(e.what());
        return;
    } catch (...) {
        cerr << "unknown exception\n";
        api.getLogger().error("unknown exception");
        return;
    }
}

#pragma GCC diagnostic pop