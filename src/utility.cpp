#include "utility.h"

#include "database.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <filesystem>

using namespace tgbot;
using namespace utils;
using namespace types;
using namespace methods;
using namespace methods::types;
using namespace std;

//void log(const Message message, bool enable_log_file, bool is_anon_chat = false);
//
//int mod(int divident, int divisor);
//
//bool is_admin(const Message message, Api& api);
//
//bool is_bot_admin(const Message message, Api& api);
//
//bool is_this_bot(const Message message, Api& api);
//
//bool is_reply(const Message message);
//
//void cut_command_message(const Message message);
//
//std::string get_any_name(const Message message);
//
//std::string get_mention(const Message message, std::string mention = "");
//
//std::size_t get_length(std::string& str);
//
//void update_database(nlohmann::json& db);



void log(const Message& message, bool enable_log_file, bool is_anon_chat) {
    try {
        auto username = message.from->username->empty()
                        ? "anonymous"
                        : *message.from->username;
        auto firstName = message.from->firstName.empty()
                         ? "anonymous"
                         : message.from->firstName;
        auto lastName = message.from->lastName->empty()
                        ? "anonymous"
                        : *message.from->lastName;
        auto nameFile = is_anon_chat
                        ? "log/anon_chat.txt"
                        : message.chat.type == ChatType::PRIVATE
                          ? "log/" + get_any_name(message) + '_' + to_string(message.chat.id) + ".txt"
                          : "log/" + *message.chat.title + '_' + to_string(message.chat.id) + ".txt";

        stringstream ss;
        ss <<
           put_time(gmtime(reinterpret_cast<const long int*>(&message.date)), "%c %Z") << " | " <<
           to_string(message.from->id) + " | " <<
           username + " | " <<
           firstName + " | " <<
           lastName + " | " <<
           '\"' + *message.text + '\"' << endl;
        if (!is_anon_chat) {
            cout << ss.str();
        }
        if (enable_log_file) {
            ofstream logFile(nameFile, ios::app | ios::out);
            logFile << ss.str();
            logFile.close();
        }

    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
    } catch (...) {
        std::cerr << "unknown exception\n";
    }
}

int mod(int divident, int divisor) {
    return (divident % divisor + divisor) % divisor;
}

bool is_admin(const Message& message, const Api& api) {
    auto status = api.getChatMember(to_string(message.chat.id), message.from->id).status;
    return status == ChatMemberStatus::CREATOR || status == ChatMemberStatus::ADMINISTRATOR;
}

bool is_bot_admin(const Message& message, const Api& api) {
    auto status = api.getChatMember(to_string(message.chat.id), api.getMe().id).status;
    return status == ChatMemberStatus::CREATOR || status == ChatMemberStatus::ADMINISTRATOR;
}

bool is_this_bot(const Message& message, const Api& api) {
    return message.chat.id == api.getMe().id;
}

bool is_reply(const Message& message) {
    return message.replyToMessage != nullptr;
}

void cut_command_message(const Message& message) {
    message.text->erase(message.entities->at(0).offset, message.entities->at(0).length);
}

string get_any_name(const Message& message) {
    return
            !message.from->username->empty()
            ? *message.from->username
            : !message.from->firstName.empty()
              ? message.from->firstName
              : !message.from->lastName->empty()
                ? *message.from->lastName
                : "anonymous";
}

string get_mention(const Message& message, string mention) {
    if (mention.empty()) {
        mention = get_any_name(message);
    }
    return '[' + mention + "](tg://user?id=" + to_string(message.from->id) + ')';
}

size_t get_length(string& str) {
    return wstring_convert<codecvt_utf8<char32_t>, char32_t>().from_bytes(str).size();
}

void update_database() {
    try {
        filesystem::copy_file("database.json", "database.json.old", filesystem::copy_options::update_existing);
        ofstream ofs("database.json", ios::out | ios::trunc);
        ofs << setw(4) << db << endl;
        ofs.close();
        cout << "database succesfully updated" << endl;

    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
    } catch (...) {
        std::cerr << "unknown exception\n";
    }
}